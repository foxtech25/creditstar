var gulp = require('gulp');
var sass = require('gulp-sass');
var minifyCSS = require('gulp-csso');
var concat = require('gulp-concat');

gulp.task('css', function(){
    return gulp.src('web/css/sass/*.scss')
        .pipe(sass())
        .pipe(minifyCSS())
        .pipe(concat('all.min.css'))
        .pipe(gulp.dest('web/css/'))
});

gulp.task('default', ['css']);