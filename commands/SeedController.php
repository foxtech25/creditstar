<?php
/**
 * Created by PhpStorm.
 * User: mbavdys
 * Date: 18.06.18
 * Time: 14:36
 */

namespace app\commands;

use yii;
use yii\console\Controller;

/**
 * Class SeedController
 * @package app\commands
 */
class SeedController extends Controller
{
    /**
     * @param string $path
     * @return array
     */
    private function loadData(string $path): array
    {
        return json_decode(
            file_get_contents($path)
        );
    }

    /**
     * @param string $table
     * @param array $field
     * @param array $data
     */
    private function setDataToDB(string $table, array $field, array $data)
    {
        Yii::$app->db
            ->createCommand()
            ->batchInsert($table, $field, $data)
            ->execute();
    }

    /**
     * Seed for User
     */
    public function actionUser()
    {
        echo 'Start' . PHP_EOL;

        $data = $this->loadData('users.json');

        $processed = [];
        foreach ($data as $datum) {
            $processed[] = [
                $datum->id,
                $datum->first_name,
                $datum->last_name,
                $datum->email,
                $datum->personal_code,
                $datum->phone,
                $datum->active,
                $datum->dead,
                $datum->lang,
            ];
        }

        $this->setDataToDB('user', [
            'id',
            'first_name',
            'last_name',
            'email',
            'personal_code',
            'phone',
            'active',
            'dead',
            'lang'
        ], $processed);

        echo 'End' . PHP_EOL;
    }

    /**
     * Seed for Loan
     */
    public function actionLoan()
    {
        echo 'Start' . PHP_EOL;

        $data = $this->loadData('loans.json');

        $processed = [];
        foreach ($data as $datum) {
            $processed[] = [
                $datum->id,
                $datum->user_id,
                $datum->amount,
                $datum->interest,
                $datum->duration,
                date('Y-m-d', $datum->start_date),
                date('Y-m-d', $datum->end_date),
                $datum->campaign,
                $datum->status,
            ];
        }

        $this->setDataToDB('loan', [
            'id',
            'user_id',
            'amount',
            'interest',
            'duration',
            'start_date',
            'end_date',
            'campaign',
            'status'
        ], $processed);

        echo 'End' . PHP_EOL;
    }
}