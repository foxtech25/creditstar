<?php
/**
 * Created by PhpStorm.
 * User: mbavdys
 * Date: 18.06.18
 * Time: 16:40
 */

namespace app\controllers;

use Yii;
use app\models\User;
use yii\web\Controller;

/**
 * Class UsersController
 * @package app\controllers
 */
class UsersController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex(): string
    {
        $users = User::find()->asArray()
            ->all();

        return $this->render('list', [
            'users' => $users
        ]);
    }

    /**
     * @param int $id
     * @return string
     */
    public function actionGet(int $id): string
    {
        $user = User::findOne($id)->toArray();

        return $this->render('get', [
            'user' => $user
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new User();

        if ($data = Yii::$app->request->post()) {
            $model->load($data);

            if ($model->validate()) {
                $model->save();

                return $this->redirect('/users');
            }
        }

        return $this->render('create', [
            'model' => $model
        ]);
    }

    /**
     * @param int $id
     * @return string|\yii\web\Response
     */
    public function actionUpdate(int $id)
    {
        $model = new User();

        if ($data = Yii::$app->request->post()) {
            $user = $model->getById($id);
            $user->load($data);

            if ($user->validate()) {
                $user->save();

                return $this->redirect('/users');
            }
        }

        return $this->render('update', [
            'model' => $model,
            'user'  => $model->getById($id)
                             ->toArray()
        ]);
    }
}