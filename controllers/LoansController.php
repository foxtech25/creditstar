<?php
/**
 * Created by PhpStorm.
 * loan: mbavdys
 * Date: 19.06.18
 * Time: 11:58
 */

namespace app\controllers;

use Yii;
use app\models\Loan;
use yii\web\Controller;

class LoansController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex(): string
    {
        $loans = Loan::find()->asArray()
            ->all();

        return $this->render('list', [
            'loans' => $loans
        ]);
    }

    /**
     * @param int $id
     * @return string
     */
    public function actionGet(int $id): string
    {
        $loan = Loan::findOne($id)->toArray();

        return $this->render('get', [
            'loan' => $loan
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Loan();

        if ($data = Yii::$app->request->post()) {
            $model->load($data);

            if ($model->validate()) {
                $model->save();

                return $this->redirect('/loans');
            }
        }

        return $this->render('create', [
            'model' => $model
        ]);
    }

    /**
     * @param int $id
     * @return string|\yii\web\Response
     */
    public function actionUpdate(int $id)
    {
        $model = new Loan();

        if ($data = Yii::$app->request->post()) {
            $loan = $model->getById($id);
            $loan->load($data);

            if ($loan->validate()) {
                $loan->save();

                return $this->redirect('/loans');
            }
        }

        return $this->render('update', [
            'model' => $model,
            'loan'  => $model->getById($id)
                ->toArray()
        ]);
    }
}