<?php
/**
 * Created by PhpStorm.
 * User: foxtech
 * Date: 18.06.18
 * Time: 23:15
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<?php $model->errors ? var_dump($model->errors) : null;?>
<?php $form = ActiveForm::begin(['id' => 'user-form']); ?>

<?= $form->field($model, 'first_name')->textInput(['autofocus' => true]) ?>
<?= $form->field($model, 'last_name')->textInput(['autofocus' => true]) ?>
<?= $form->field($model, 'email') ?>
<?= $form->field($model, 'personal_code') ?>
<?= $form->field($model, 'phone') ?>
<?= $form->field($model, 'lang')->dropDownList([
    'est' => 'Estonian',
    'rus' => 'Russian',
]); ?>


<div class="form-group">
    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
</div>

<?php ActiveForm::end(); ?>
