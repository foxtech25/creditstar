<?php
/**
 * Created by PhpStorm.
 * User: mbavdys
 * Date: 18.06.18
 * Time: 17:25
 */
?>
<div class="table-container">
    <div class="row">
        <div class="col-lg-2">First Name</div>
        <div class="col-lg-2">Last Name</div>
        <div class="col-lg-2">Email</div>
        <div class="col-lg-2">Code</div>
        <div class="col-lg-2">Phone</div>
        <div class="col-lg-2">Lang code</div>
        <hr>

        <?php foreach ($users as $user): ?>
            <div class="col-lg-2"><?= $user['first_name']; ?></div>
            <div class="col-lg-2"><?= $user['last_name']; ?></div>
            <div class="col-lg-2"><?= $user['email']; ?></div>
            <div class="col-lg-2"><?= $user['personal_code']; ?></div>
            <div class="col-lg-2"><?= $user['phone']; ?></div>
            <div class="col-lg-2"><?= $user['lang']; ?></div>
            <div class="clearfix"></div>
            <hr>
        <?php endforeach;?>

    </div>
</div>