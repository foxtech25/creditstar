<?php
/**
 * Created by PhpStorm.
 * User: foxtech
 * Date: 18.06.18
 * Time: 23:15
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

var_dump($user);
?>
<?php $model->errors ? var_dump($model->errors) : null;?>
<?php $form = ActiveForm::begin(['id' => 'user-form']); ?>

<?= $form->field($model, 'first_name')->textInput(['autofocus' => true, 'value' => $user['first_name']]); ?>

<?= $form->field($model, 'last_name')->textInput(['autofocus' => true, 'value' => $user['last_name']]); ?>

<?= $form->field($model, 'email')->textInput(['value' => $user['email']]); ?>

<?= $form->field($model, 'personal_code')->textInput(['value' => $user['personal_code']]); ?>

<?= $form->field($model, 'phone')->textInput(['value' => $user['phone']]); ?>

<?= $form->field($model, 'lang')->dropDownList([
    'est' => 'Estonian',
    'rus' => 'Russian',
]); ?>


<div class="form-group">
    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
</div>

<?php ActiveForm::end(); ?>
