<?php
/**
 * Created by PhpStorm.
 * User: foxtech
 * Date: 18.06.18
 * Time: 23:15
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

var_dump($loan);
?>
<?php $model->errors ? var_dump($model->errors) : null;?>
<?php $form = ActiveForm::begin(['id' => 'loan-form']); ?>

<?= $form->field($model, 'user_id')->textInput(['type' => 'number', 'value' => $loan['user_id']]) ?>
<?= $form->field($model, 'amount')->textInput(['type' => 'number', 'value' => $loan['amount']]) ?>
<?= $form->field($model, 'interest')->textInput(['value' => $loan['interest']]); ?>
<?= $form->field($model, 'duration')->textInput(['value' => $loan['duration']]); ?>
<?= $form->field($model, 'start_date')->textInput(['value' => $loan['start_date']]); ?>
<?= $form->field($model, 'end_date')->textInput(['value' => $loan['end_date']]); ?>
<?= $form->field($model, 'campaign')->textInput(['value' => $loan['campaign']]); ?>
<?= $form->field($model, 'status')->textInput(['value' => $loan['status']]); ?>

    <div class="form-group">
    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
</div>

<?php ActiveForm::end(); ?>