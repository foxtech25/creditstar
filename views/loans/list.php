<?php
/**
 * Created by PhpStorm.
 * User: mbavdys
 * Date: 19.06.18
 * Time: 12:30
 */
?>

<div class="table-container">
    <div class="row">
        <div class="col-lg-2">User id</div>
        <div class="col-lg-2">Amount</div>
        <div class="col-lg-2">Interest</div>
        <div class="col-lg-2">Duration</div>
        <div class="col-lg-2">Start date</div>
        <div class="col-lg-2">End Date</div>
        <hr>

        <?php foreach ($loans as $loan): ?>
            <div class="col-lg-2"><?= $loan['user_id']; ?></div>
            <div class="col-lg-2"><?= $loan['amount']; ?></div>
            <div class="col-lg-2"><?= $loan['interest']; ?></div>
            <div class="col-lg-2"><?= $loan['duration']; ?></div>
            <div class="col-lg-2"><?= $loan['start_date']; ?></div>
            <div class="col-lg-2"><?= $loan['end_date']; ?></div>
            <div class="clearfix"></div>
            <hr>
        <?php endforeach;?>
    </div>
</div>