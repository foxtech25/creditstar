<?php
/**
 * Created by PhpStorm.
 * User: foxtech
 * Date: 18.06.18
 * Time: 23:15
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<?php $model->errors ? var_dump($model->errors) : null;?>
<?php $form = ActiveForm::begin(['id' => 'user-form']); ?>

<?= $form->field($model, 'user_id')->textInput(['type' => 'number']) ?>
<?= $form->field($model, 'amount')->textInput(['type' => 'number']) ?>
<?= $form->field($model, 'interest') ?>
<?= $form->field($model, 'duration') ?>
<?= $form->field($model, 'start_date') ?>
<?= $form->field($model, 'end_date') ?>
<?= $form->field($model, 'campaign') ?>
<?= $form->field($model, 'status') ?>

<div class="form-group">
    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
</div>

<?php ActiveForm::end(); ?>